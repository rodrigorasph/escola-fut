var app = angular.module('application', ['ngRoute']);

app.run(function ($rootScope, $location) {
	$rootScope.loadView = function(url) {
		$location.path(url);
	};
});
