app.config(function($routeProvider, $locationProvider){
	$routeProvider
	.when('/professores',{
		templateUrl: 'views/professores.html'
	})
	.when('/professoresabas',{
		templateUrl: 'views/professoresabas.html'
	})
	.when('/contato',{
		templateUrl: 'views/contato.html'
	})
	.when('/sobre',{
		templateUrl: 'views/sobre.html'
	})
	.otherwise({
		redirectTo: '/professores'
	});
	$locationProvider.html5Mode(true);
});
