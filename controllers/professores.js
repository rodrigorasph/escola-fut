app.controller('professoresController', function ($scope){

	$scope.professores = [

	];

	$scope.professor = {};

	$scope.selecionaProfessor = function(professorSelecionado){
		$scope.professor = professorSelecionado;
	};
	$scope.limparCampos = function(){
		$scope.professor = {};
	};
	$scope.salvarProfessor = function(){
		$scope.professores.push($scope.professor);
		$scope.limparCampos();
	};
	$scope.excluirProfessor = function(){
		$scope.professores.splice($scope.professores.indexOf($scope.professor),1);
		$scope.professor = {};
	};
	
});
