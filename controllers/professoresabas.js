app.controller('professoresabasController', function($scope, $route, $timeout){
	
	$scope.professores = [];

	$scope.professor = {};

	$scope.selecionaProfessor = function(professorSelecionado){
		$scope.professor = professorSelecionado;
	};
	$scope.limparCampos = function(){
		$scope.professor = {};
	};
	$scope.salvarProfessor = function(){
		$scope.professores.push($scope.professor);
		$scope.limparCampos();
		$scope.$apply();
	};
	$scope.excluirProfessor = function(){
		$scope.professores.splice($scope.professores.indexOf($scope.professor),1);
		$scope.professor = {};
	};
});

